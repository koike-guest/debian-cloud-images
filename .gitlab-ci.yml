---
variables:
  CLOUD_IMAGE_VERSION: dev
  CLOUD_IMAGE_NAME: debian-${CLOUD_IMAGE_RELEASE}-${CLOUD_IMAGE_VERSION}-${CLOUD_VENDOR}-${CLOUD_ARCH}

.build: &build
  stage: build
  image: debian:stretch-backports
  script:
    - apt-get update
    - >
      apt-get install --no-install-recommends -y -t stretch-backports
      ca-certificates debsums dosfstools fai-server fai-setup-storage make python3 sudo qemu-utils udev
    # Some of our build environments run with SELinux enabled, make sure it is detected by all the tools
    - if [ -d /sys/fs/selinux ]; then mount -t selinuxfs none /sys/fs/selinux; mkdir -p /etc/selinux; touch /etc/selinux/config; fi
    - bin/build ${CLOUD_RELEASE} ${CLOUD_VENDOR} ${CLOUD_ARCH} ${CLOUD_IMAGE_NAME} ${CLOUD_OPTIONS}
    - 'xz -vk5T0 *.tar'
  artifacts:
    name: ${CLOUD_IMAGE_NAME}
    expire_in: 7 days
    paths:
      - '*.info'
      - '*.manifest.yml'
      - '*.tar.xz'

####
# Different selections when builds will be done
####

.only-pushes: &only-pushes
  only:
    - pushes

.only-triggers-official: &only-triggers-official
  only:
    - triggers@cloud-team/debian-cloud-images

.only-web: &only-web
  only:
    - web

####
# Builds for developer uploads pushes
####

azure stretch build: &build-azure-stretch
  <<: *build
  <<: *only-web
  tags:
    - x86
  variables:
    CLOUD_ARCH: amd64
    CLOUD_RELEASE: stretch
    CLOUD_IMAGE_RELEASE: 9
    CLOUD_VENDOR: azure

azure stretch-backports build: &build-azure-stretch-backports
  <<: *build
  <<: *only-web
  tags:
    - x86
  variables:
    CLOUD_ARCH: amd64
    CLOUD_RELEASE: stretch-backports
    CLOUD_IMAGE_RELEASE: 9-backports
    CLOUD_VENDOR: azure

azure buster build: &build-azure-buster
  <<: *build
  <<: *only-web
  tags:
    - x86
  variables:
    CLOUD_ARCH: amd64
    CLOUD_RELEASE: buster
    CLOUD_IMAGE_RELEASE: 10
    CLOUD_VENDOR: azure
  # no waagent in buster
  allow_failure: true

azure sid build: &build-azure-sid
  <<: *build
  <<: *only-pushes
  tags:
    - x86
  variables:
    CLOUD_ARCH: amd64
    CLOUD_RELEASE: sid
    CLOUD_IMAGE_RELEASE: sid
    CLOUD_VENDOR: azure

ec2 stretch build: &build-ec2-stretch
  <<: *build
  <<: *only-web
  tags:
    - x86
  variables:
    CLOUD_ARCH: amd64
    CLOUD_RELEASE: stretch
    CLOUD_IMAGE_RELEASE: 9
    CLOUD_VENDOR: ec2

ec2 buster build: &build-ec2-buster
  <<: *build
  <<: *only-web
  tags:
    - x86
  variables:
    CLOUD_ARCH: amd64
    CLOUD_RELEASE: buster
    CLOUD_IMAGE_RELEASE: 10
    CLOUD_VENDOR: ec2

ec2 sid build: &build-ec2-sid
  <<: *build
  <<: *only-pushes
  tags:
    - x86
  variables:
    CLOUD_ARCH: amd64
    CLOUD_RELEASE: sid
    CLOUD_IMAGE_RELEASE: sid
    CLOUD_VENDOR: ec2

gce stretch build: &build-gce-stretch
  <<: *build
  <<: *only-web
  tags:
    - x86
  variables:
    CLOUD_ARCH: amd64
    CLOUD_RELEASE: stretch
    CLOUD_IMAGE_RELEASE: 9
    CLOUD_VENDOR: gce

gce buster build: &build-gce-buster
  <<: *build
  <<: *only-web
  tags:
    - x86
  variables:
    CLOUD_ARCH: amd64
    CLOUD_RELEASE: buster
    CLOUD_IMAGE_RELEASE: 10
    CLOUD_VENDOR: gce
  # uses external stuff
  allow_failure: true

gce sid build: &build-gce-sid
  <<: *build
  <<: *only-pushes
  tags:
    - x86
  variables:
    CLOUD_ARCH: amd64
    CLOUD_RELEASE: sid
    CLOUD_IMAGE_RELEASE: sid
    CLOUD_VENDOR: gce
  # uses external stuff
  allow_failure: true

nocloud stretch amd64 build: &build-nocloud-stretch-amd64
  <<: *build
  <<: *only-web
  tags:
    - x86
  variables:
    CLOUD_ARCH: amd64
    CLOUD_RELEASE: stretch
    CLOUD_IMAGE_RELEASE: 9
    CLOUD_VENDOR: nocloud

nocloud buster amd64 build: &build-nocloud-buster-amd64
  <<: *build
  <<: *only-web
  tags:
    - x86
  variables:
    CLOUD_ARCH: amd64
    CLOUD_RELEASE: buster
    CLOUD_IMAGE_RELEASE: 10
    CLOUD_VENDOR: nocloud

nocloud sid amd64 build: &build-nocloud-sid-amd64
  <<: *build
  <<: *only-pushes
  tags:
    - x86
  variables:
    CLOUD_ARCH: amd64
    CLOUD_RELEASE: sid
    CLOUD_IMAGE_RELEASE: sid
    CLOUD_VENDOR: nocloud

nocloud stretch amd64-efi build: &build-nocloud-stretch-amd64-efi
  <<: *build
  <<: *only-web
  tags:
    - x86
  variables:
    CLOUD_ARCH: amd64-efi
    CLOUD_RELEASE: stretch
    CLOUD_IMAGE_RELEASE: 9
    CLOUD_VENDOR: nocloud

nocloud buster amd64-efi build: &build-nocloud-buster-amd64-efi
  <<: *build
  <<: *only-web
  tags:
    - x86
  variables:
    CLOUD_ARCH: amd64-efi
    CLOUD_RELEASE: buster
    CLOUD_IMAGE_RELEASE: 10
    CLOUD_VENDOR: nocloud

nocloud sid amd64-efi build: &build-nocloud-sid-amd64-efi
  <<: *build
  <<: *only-pushes
  tags:
    - x86
  variables:
    CLOUD_ARCH: amd64-efi
    CLOUD_RELEASE: sid
    CLOUD_IMAGE_RELEASE: sid
    CLOUD_VENDOR: nocloud

####
# Official builds done on Debian hardware
####

azure stretch build official:
  <<: *build-azure-stretch
  <<: *only-triggers-official
  tags:
    - debian-cloud-images-build
    - x86

azure stretch-backports build official:
  <<: *build-azure-stretch-backports
  <<: *only-triggers-official
  tags:
    - debian-cloud-images-build
    - x86

azure buster build official:
  <<: *build-azure-buster
  <<: *only-triggers-official
  tags:
    - debian-cloud-images-build
    - x86

azure sid build official:
  <<: *build-azure-sid
  <<: *only-triggers-official
  tags:
    - debian-cloud-images-build
    - x86

ec2 stretch build official:
  <<: *build-ec2-stretch
  <<: *only-triggers-official
  tags:
    - debian-cloud-images-build
    - x86

ec2 buster build official:
  <<: *build-ec2-buster
  <<: *only-triggers-official
  tags:
    - debian-cloud-images-build
    - x86

ec2 sid build official:
  <<: *build-ec2-sid
  <<: *only-triggers-official
  tags:
    - debian-cloud-images-build
    - x86

gce stretch build official:
  <<: *build-gce-stretch
  <<: *only-triggers-official
  tags:
    - debian-cloud-images-build
    - x86

gce buster build official:
  <<: *build-gce-buster
  <<: *only-triggers-official
  tags:
    - debian-cloud-images-build
    - x86

gce sid build official:
  <<: *build-gce-sid
  <<: *only-triggers-official
  tags:
    - debian-cloud-images-build
    - x86

nocloud stretch amd64 build official:
  <<: *build-nocloud-stretch-amd64
  <<: *only-triggers-official
  tags:
    - debian-cloud-images-build
    - x86

nocloud buster amd64 build official:
  <<: *build-nocloud-buster-amd64
  <<: *only-triggers-official
  tags:
    - debian-cloud-images-build
    - x86

nocloud sid amd64 build official:
  <<: *build-nocloud-sid-amd64
  <<: *only-triggers-official
  tags:
    - debian-cloud-images-build
    - x86

nocloud stretch amd64-efi build official:
  <<: *build-nocloud-stretch-amd64-efi
  <<: *only-triggers-official
  tags:
    - debian-cloud-images-build
    - x86

nocloud buster amd64-efi build official:
  <<: *build-nocloud-buster-amd64-efi
  <<: *only-triggers-official
  tags:
    - debian-cloud-images-build
    - x86

nocloud sid amd64-efi build official:
  <<: *build-nocloud-sid-amd64-efi
  <<: *only-triggers-official
  tags:
    - debian-cloud-images-build
    - x86
